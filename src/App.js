import React from 'react';
import logo from './logo.svg';
import { ToastContainer, toast } from 'react-toastify';
import './App.css';
import 'react-toastify/dist/ReactToastify.css';
import Course from './course/Course';

function App() {
  return (
    <div >
      <ToastContainer />
      <Course />
    </div>
  );
}

export default App;
