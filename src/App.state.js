
export const courseListDefault = () => {
    return {
        list: [
            { id: 1, name: 'Technical Report Writing' },
            { id: 2, name: 'English Literature' },
            { id: 3, name: 'Computer Sciences' }
        ]
    }
}


const states = (function states() {
    return {
        courseList: courseListDefault(),
        additonal:''
    }
})()
export default states;
