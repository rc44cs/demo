import React, { Component, Fragment } from 'react';
import states from './Course.state';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Course extends React.Component {
    state = states

    componentDidMount = () => {
        console.log(this.state)

        this.onSelectCourse(this.state.course.courseID)
    }


    onAddtional = (e) => {
        let additonal = e.target.value;
        this.setState({ additonal });
    }

    onSubmit = () => {
        debugger

        if (!this.state.course.courseID) {
            toast.warn('Course is required')
            return;
        }
        if (!this.state.course.subject) {
            toast.warn('Subject is required')
            return;
        }

        if (!this.state.course.startDate) {
            toast.warn('Start date is required')
            return;
        }

        if (this.state.course.startDate && (this.state.course.startDate !== '2019-12-20' && this.state.course.startDate !== '2020-01-15' && this.state.course.startDate !== '2020-02-01')) {
            toast.warn('Your selected course and subject is not offered beginning from your selected date')
            return;
        }

        if (this.state.course.note && (this.state.course.note.length > 300 || this.state.course.note.length < 20)) {
            toast.warn('Note must be greater than 500 & less than 20')
            return;
        }
        this.setState({isLoading:true})
        setTimeout(() => {
            this.setState({isLoading:false})
            toast.success('Saved successfully')
          }, 3000);
    }

    onHandleChange = (name, value) => {
        debugger
        let { course } = this.state;
        course[name] = value;
        this.setState({ course });
    }

    onSelectCourse = (value) => {
        let { subject, course } = this.state;
        course.courseID = value
        subject.list.forEach(sub => {
            sub.isShow = false;
            if (sub.courseId === value) {
                sub.isShow = true
            }
        })
        this.setState({ subject, course })
    }


    render() {
        return (
            <Fragment>
                <div style={{ position: 'absolute', left: '50%', top: '5%',fontSize:35}}>{'Registration'}</div>
                <div className="col-sm-12" style={{marginTop:100,marginLeft:440,background:'#94b8b8',width:'50%'}}>
                    <div style={{ padding: 10,fontSize:20,fontWeight:'bold' }}>
                        Courses
                    </div>
                    <div className="col-sm-12">
                        {this.state.courseList.list.map((list => {
                            return (
                                <div className="form-check-inline" >
                                    <label className="form-check-label" >
                                        <input type="radio" className="form-check-input"
                                            checked={this.state.course.courseID === list.id}
                                            onClick={() => this.onSelectCourse(list.id)}
                                            id="radio1" name="optradio" value={this.state.course.courseID} />{list.name}
                                    </label>
                                </div>
                            )
                        }))}
                    </div>
                    {this.state.course.courseID > 0 ?
                        <div style={{ padding: 10 }}>
                            Subject
                 </div>
                        : ''}

                    <div className="col-sm-12">
                        {this.state.subject.list.map((list => {
                            return (
                                <div>
                                    {list.isShow ?

                                        <div className="form-check-inline" >

                                            <label className="form-check-label" >
                                                <input type="radio" className="form-check-input"
                                                    checked={this.state.course.subject === list.id}
                                                    onClick={() => this.onHandleChange('subject', list.id)}
                                                    id="radioc1" name="optraddio" value={this.state.course.subject} />{list.name}
                                            </label>

                                        </div>
                                        : ''}
                                </div>


                            )
                        }))}
                    </div>
                    <div style={{ padding: 10,fontSize:20,fontWeight:'bold' }}>
                        Start Date
                    </div>
                    <div className="col-sm-12">
                        <input type="date" id="course-date" onChange={(event) => this.onHandleChange('startDate', event.target.value)} name="course-date" value={this.state.course.startDate} />

                    </div>
                    <div style={{ padding: 10,fontSize:20,fontWeight:'bold' }}>Additional Note</div>
                    <div className="col-sm-4" >
                        <textarea className="form-control" rows="4" id="comment" value={this.state.course.note} onChange={(event) => this.onHandleChange('note', event.target.value)} maxLength="500">{this.state.additonal}</textarea>


                    </div>
                    <div style={{ padding: 10 }}>
                        <button className="btn btn-success" onClick={() => this.onSubmit()}>
                            <i  className={`${this.state.isLoading ? "fa fa-refresh fa-spin" : ""}`}></i>Submit
                        </button>
                    </div>



                </div>
            </Fragment>
        )
    }
}
export default Course