
export const courseListDefault = () => {
    return {
        list: [
            { id: 1, name: 'Technical Report Writing' },
            { id: 2, name: 'English Literature' },
            { id: 3, name: 'Computer Sciences' }
        ]
    }
}

export const subjectDefault = () => {
    return {
        list: [
            { id: 5, name: 'Short Reports', courseId: 1, isShow: false },
            { id: 6, name: 'Annual Reports ', courseId: 1, isShow: false },
            { id: 7, name: 'Presentations', courseId: 1, isShow: false },
            { id: 5, name: 'Poetry', courseId: 2, isShow: false },
            { id: 6, name: 'Short Stories', courseId: 2, isShow: false },
            { id: 7, name: 'Drama', courseId: 2, isShow: false },
            { id: 5, name: 'Web Development', courseId: 3, isShow: false },
            { id: 6, name: 'Desktop Software Development', courseId: 3, isShow: false },
            { id: 7, name: 'Research and Analysis', courseId: 3, isShow: false }
        ]
    }
}

export const courseDefault = () => {
    return {
        courseID: 0,
        subject: 0,
        note: '',
        startDate: ''
    }
}


const states = (function states() {
    return {
        course: courseDefault(),
        subject: subjectDefault(),
        courseList: courseListDefault(),
        isLoading: false
    }
})()
export default states;
